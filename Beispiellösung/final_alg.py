#!/usr/bin/env python3

from bottle import post, request, run, BaseRequest
import heapq
import copy


class City:
    """ Contains the information about a city """

    def __init__(self, city_json):

        self.name = city_json["name"]
        self.latitude = city_json["latitude"]
        self.longitude = city_json["longitude"]
        self.population = city_json["population"]
        self.connections = city_json["connections"]

        self.economy = rating2ints(city_json["economy"])
        self.government = rating2ints(city_json["government"])
        self.hygiene = rating2ints(city_json["hygiene"])
        self.awareness = rating2ints(city_json["awareness"])

        self.pathogen_name = ""
        self.prevalence = 0
        self.other_events = []
        self.med_deployed = []
        self.vacc_deployed = []

        self.uprising = False
        self.antivaxx = False
        self.bioTerror = False

        self.quarantine = False

        self.influence = False
        self.elections = False
        self.campaign = False
        self.hygiene = False

        if "events" not in city_json:
            self.events = []
        else:
            self.events = city_json["events"]

        for event in self.events:
            if event["type"] == "outbreak":
                pathogen_json = event["pathogen"]
                self.pathogen_name = pathogen_json["name"]
                self.prevalence = event["prevalence"]
            elif event["type"] == "medicationDeployed":
                self.med_deployed.append(event["pathogen"]["name"])
            elif event["type"] == "vaccineDeployed":
                self.vacc_deployed.append(event["pathogen"]["name"])
            elif event["type"] == "antiVaccinationism":
                self.antivaxx = True
            elif event["type"] == "uprising":
                self.uprising = True
            elif event["type"] == "bioTerrorism":
                self.bioTerror = True
            elif event["type"] == "influenceExerted":
                self.influence = True
            elif event["type"] == "quarantine":
                self.quarantine = True
            elif event["type"] == "campaignLaunched":
                self.campaign = True
            elif event["type"] == "hygienicMeasuresApplied":
                self.hygiene = True
            elif event["type"] == "electionsCalled":
                self.elections = True
            else:
                self.other_events.append(event)

        self.city_rating = self.rate_city()

    def rate_city(self):
        """ Gives the city a rating according to its population"""
        rating = self.population
        return rating


class Event:
    """Contains information about an event"""

    def __init__(self, event_json):

        self.type = event_json["type"]

        if self.type == "pathogenEncountered":
            self.round = event_json["round"]
        else:
            self.round = event_json["sinceRound"]

        if "untilRound" in event_json:
            self.untilRound = event_json["untilRound"]


class Pathogen(Event):
    """Contains the information about a pathogen"""

    def __init__(self, event_json):
        Event.__init__(self, event_json)
        pathogen_json = event_json["pathogen"]
        self.name = pathogen_json["name"]

        self.infectivity = rating2ints(pathogen_json["infectivity"])
        self.mobility = rating2ints(pathogen_json["mobility"])
        self.duration = rating2ints(pathogen_json["duration"])
        self.lethality = rating2ints(pathogen_json["lethality"])

        self.affected_population = 0
        self.infected_population = 0

        self.threat = True
        self.meds_needed = True
        self.pathogen_rating = self.rate_pathogen()

        # 0: not in development or available, 1: in development, 2: available
        self.medication = 0
        self.vaccine = 0

        self.city_names = []

    def rate_pathogen(self):
        """Rates the pathogen using its properties"""
        return (3 * self.lethality + self.infectivity + self.mobility + self.duration) * self.threat


class PandemicGame:
    """Contains the game information"""

    def __init__(self, game_json):

        self.round = game_json["round"]
        self.outcome = game_json["outcome"]
        self.points = game_json["points"]

        self.total_population = 0
        self.infected_population = 0
        self.cities = {}
        for city in game_json["cities"]:
            city_json = game_json["cities"][city]
            self.cities[city] = City(city_json)
            self.total_population += city_json["population"]

        self.infected_cities = self.get_infected_cities()
        self.city_priority_queue = self.rate_all_cities(self.cities)

        self.pathogens = {}
        self.events = []
        self.other_events = []
        self.quarantined_paths = 0

        self.panic = False
        self.ecocrisis = False

        cure_events = ["medicationInDevelopment", "medicationAvailable", "vaccineInDevelopment", "vaccineAvailable"]
        if "events" not in game_json:
            self.events = []
        else:
            self.events = game_json["events"]

        for event in self.events:
            event_name = event["type"]
            if event_name == "pathogenEncountered":
                pathogen_json = event["pathogen"]
                self.pathogens[pathogen_json["name"]] = Pathogen(event)

            elif event_name in cure_events:
                cure_json = event["pathogen"]
                cure_name = cure_json["name"]

                if "medication" in event_name:
                    if "InDevelopment" in event_name:
                        self.pathogens[cure_name].medication = 1
                    if "Available" in event_name:
                        self.pathogens[cure_name].medication = 2

                elif "vaccine" in event_name:
                    if "InDevelopment" in event_name:
                        self.pathogens[cure_name].vaccine = 1
                    if "Available" in event_name:
                        self.pathogens[cure_name].vaccine = 2
            elif event_name == "largeScalePanic":
                self.panic = True
            elif event_name == "economicCrisis":
                self.ecocrisis = True
            else:
                self.other_events.append(event)
        for city_name in self.cities:
            city_pathogen = self.cities[city_name].pathogen_name
            if city_pathogen != "":
                self.pathogens[city_pathogen].city_names.append(city_name)
                self.pathogens[city_pathogen].affected_population += self.cities[city_name].population
                infected_population_in_city = int(self.cities[city_name].population * self.cities[city_name].prevalence)
                self.pathogens[city_pathogen].infected_population += infected_population_in_city
                self.infected_population += infected_population_in_city

        # Update Rating
        for p in self.pathogens:
            affected_cities = self.pathogens[p].city_names
            if len(affected_cities) == 1 and self.cities[affected_cities[0]].quarantine:
                self.pathogens[p].threat = False
                self.quarantined_paths += 1

                if self.cities[affected_cities[0]].population / self.total_population < 0.05:
                    self.pathogens[p].meds_needed = False

            self.pathogens[p].pathogen_rating = self.pathogens[p].rate_pathogen()

        self.pathogen_priority_queue = self.pathogen_prio(self.pathogens)

        self.errors = []
        if "error" in game_json:
            self.errors = game_json["error"]
            print("Errors:", game_json["error"])

    def get_infected_cities(self):
        """Returns the list of names of cities, which are infected"""
        inf_cities = []
        for city_name in self.cities:
            city = self.cities[city_name]
            if city.pathogen_name != "":
                inf_cities.append(city.name)
        return inf_cities

    def rate_all_cities(self, cities):
        """Gets a list of city names and returns a priority queue with its names"""
        city_queue = []
        for city_name in cities:
            pp = self.cities[city_name]
            heapq.heappush(city_queue, (-pp.city_rating, pp.name))
        return city_queue

    def pathogen_prio(self, pathogens):
        """ Gets a list of pathogen names and return a priority queue with its names"""
        pat_queue = []
        for pathogen_name in pathogens:
            pp = self.pathogens[pathogen_name]
            heapq.heappush(pat_queue, (-pp.pathogen_rating, pp.name))
        return pat_queue

    def strategy_high_stats(self):
        """Strategy to increase te city_stats"""
        infected_cities = {}
        for city in self.infected_cities:
            infected_cities[city] = self.cities[city]

        infected_cities_q = self.rate_all_cities(infected_cities)

        while self.points >= 3 and infected_cities_q:

            city_name = heapq.heappop(infected_cities_q)[1]
            city_object = self.cities[city_name]

            city_stats_sum = city_object.economy + city_object.awareness + city_object.hygiene + city_object.government
            city_stats = [city_object.economy, city_object.awareness, city_object.hygiene, city_object.government]

            if city_stats_sum < 8:
                if self.ecocrisis and city_stats[0] < 2:
                    lowest_stat_index = 0
                if self.panic and city_stats[1] < 2:
                    lowest_stat_index = 1
                if self.cities[city_name].uprising and city_stats[3] < 2:
                    lowest_stat_index = 3
                if not (self.ecocrisis or self.panic or self.cities[city_name].uprising):
                    lowest_stat_index = city_stats.index(min(city_stats))
                else:
                    lowest_stat_index = -1

                if lowest_stat_index == 0:
                    return self.action_exert_influence(city_name)
                elif lowest_stat_index == 1:
                    return self.action_launch_campaign(city_name)
                elif lowest_stat_index == 2:
                    return self.action_apply_hygienic_measures(city_name)
                elif lowest_stat_index == 3:
                    return self.action_call_elections(city_name)

        if (not infected_cities_q) or self.points < 3:
            return self.action_end_round()

    def final_strategy(self):
        """Combination of Quarantine, Development and Deployment of Cures and high city stats"""
        vaccs_avail = 0
        meds_avail = 0
        vaccs_devel = 0
        meds_devel = 0
        num_meds_needed = 0

        save_points = 40
        for pathogen in self.pathogens:
            if self.pathogens[pathogen].medication == 2:
                meds_avail += 1
            if self.pathogens[pathogen].vaccine == 2:
                vaccs_avail += 1
            if self.pathogens[pathogen].medication == 1:
                meds_devel += 1
            if self.pathogens[pathogen].vaccine == 1:
                vaccs_devel += 1

            num_meds_needed += self.pathogens[pathogen].meds_needed

        # Qurantine
        pathogen_q = copy.deepcopy(self.pathogen_priority_queue)
        not_infected_cities = len(self.cities) - len(self.infected_cities)
        while pathogen_q and self.points >= 40 and not_infected_cities > 1:
            highest_path_name = heapq.heappop(pathogen_q)[1]
            path_cities = self.pathogens[highest_path_name].city_names
            if len(path_cities) == 1 and (not self.cities[path_cities[0]].quarantine):
                num_rounds = (self.points - 20) // 10
                return self.action_quarantine(self.pathogens[highest_path_name].city_names[0], num_rounds)

        # Development of cures
        pathogen_q = copy.deepcopy(self.pathogen_priority_queue)
        while pathogen_q and self.points >= save_points + 40 and (vaccs_devel + vaccs_avail < len(self.pathogens) - 1):
            highest_path_name = heapq.heappop(pathogen_q)[1]
            if self.pathogens[highest_path_name].vaccine == 0 and len(
                    self.pathogens[highest_path_name].city_names) > 1:
                return self.action_develop_vaccine(highest_path_name)

        while pathogen_q and self.points >= save_points + 20 and (vaccs_devel + vaccs_avail >= len(self.pathogens) - 1):
            highest_path_name = heapq.heappop(pathogen_q)[1]
            if self.pathogens[highest_path_name].medication == 0 and self.pathogens[highest_path_name].meds_needed:
                return self.action_develop_medication(highest_path_name)

        # Deployment of cures
        city_q = copy.deepcopy(self.city_priority_queue)
        enough_cures = meds_avail == num_meds_needed and vaccs_avail == len(self.pathogens) - 1
        while city_q and enough_cures and self.points >= save_points + 5:
            pathogen_q = copy.deepcopy(self.pathogen_priority_queue)
            highest_city_name = heapq.heappop(city_q)[1]
            highest_city = self.cities[highest_city_name]
            city_pathogen = self.cities[highest_city_name].pathogen_name

            all_vaccs_deployed = len(self.cities[highest_city_name].vacc_deployed) >= len(self.pathogens) - 1
            if all_vaccs_deployed and self.points >= save_points + 10 and city_pathogen != "" and highest_city.prevalence > 0.5 and \
                    self.pathogens[city_pathogen].medication == 2:
                return self.action_deploy_medication(city_pathogen, highest_city_name)

            if len(self.cities[highest_city_name].vacc_deployed) <= len(self.pathogens) - 1:
                while pathogen_q:
                    highest_pathogen_name = heapq.heappop(pathogen_q)[1]

                    if (highest_pathogen_name not in self.cities[
                        highest_city_name].vacc_deployed) and self.points >= save_points + 5 and self.pathogens[
                        highest_pathogen_name].vaccine == 2 and (not highest_city.antivaxx):
                        return self.action_deploy_vaccine(highest_pathogen_name, highest_city_name)

        # Improvement of the city-stats
        if enough_cures and self.points >= save_points + 10 + 3:
            return self.strategy_high_stats()

        elif (not enough_cures) and self.points >= save_points + 40 + 3:
            return self.strategy_high_stats()
        return self.action_end_round()

    """ All possible actions:"""

    @staticmethod
    def action_end_round():
        return {"type": "endRound"}

    # Cost: 10 × Anzahl Runden + 20
    @staticmethod
    def action_quarantine(city_name, rounds):
        return {"type": "putUnderQuarantine", "city": city_name, "rounds": rounds}

    # Cost: 5 × Anzahl Runden + 15
    @staticmethod
    def action_close_airport(city_name, rounds):
        return {"type": "closeAirport", "city": city_name, "rounds": rounds}

    # Cost: 3 × Anzahl Runden + 3
    @staticmethod
    def action_close_connection(from_city, to_city, rounds):
        return {"type": "closeConnection", "fromCity": from_city, "toCity": to_city, "rounds": rounds}

    # Cost: 40
    @staticmethod
    def action_develop_vaccine(pathogen_name):
        return {"type": "developVaccine", "pathogen": pathogen_name}

    # Cost: 5
    @staticmethod
    def action_deploy_vaccine(pathogen_name, city_name):
        return {"type": "deployVaccine", "pathogen": pathogen_name, "city": city_name}

    # Cost: 20
    @staticmethod
    def action_develop_medication(pathogen_name):
        return {"type": "developMedication", "pathogen": pathogen_name}

    # Cost: 10
    @staticmethod
    def action_deploy_medication(pathogen_name, city_name):
        return {"type": "deployMedication", "pathogen": pathogen_name, "city": city_name}

    # Cost: 3
    @staticmethod
    def action_exert_influence(city_name):
        return {"type": "exertInfluence", "city": city_name}

    # Cost: 3
    @staticmethod
    def action_call_elections(city_name):
        return {"type": "callElections", "city": city_name}

    # Cost: 3
    @staticmethod
    def action_apply_hygienic_measures(city_name):
        return {"type": "applyHygienicMeasures", "city": city_name}

    # Cost: 3
    @staticmethod
    def action_launch_campaign(city_name):
        return {"type": "launchCampaign", "city": city_name}


def rating2ints(rating):
    """Converts the properties of the cities and pathogens to integers """
    rating_list = ["--", "-", "o", "+", "++"]
    return rating_list.index(rating)


wins = 0
losses = 0


@post("/")
def index():
    """Returns the chosen action"""

    global wins
    global losses
    game = request.json
    game_class = PandemicGame(game)

    if not (game_class.outcome == "win" or game_class.outcome == "loss"):
        action = game_class.final_strategy()
        return action
    else:
        if game_class.outcome == "win":
            wins += 1
        if game_class.outcome == "loss":
            losses += 1
        print(wins, "/", wins + losses, " wins after ", game_class.round, "rounds")


def main():
    BaseRequest.MEMFILE_MAX = 10 * 1024 * 1024
    run(host="0.0.0.0", port=50123, quiet=True)


if __name__ == '__main__':
    main()
