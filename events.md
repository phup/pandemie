# Events


## General events
```json
{'type': 'outbreak', 'pathogen': {'name': 'Admiral Trips', 'infectivity': '++', 'mobility': '+', 'duration': '-', 'lethality': '++'}, 'prevalence': 0.9701818181818181, 'sinceRound': 2}
{'type': 'largeScalePanic', 'sinceRound': 4}
{'type': 'economicCrisis', 'sinceRound': 16}
[{'type': 'medicationInDevelopment', 'pathogen': {'name': 'Admiral Trips', 'infectivity': '++', 'mobility': '+', 'duration': '-', 'lethality': '++'}, 'sinceRound': 1, 'untilRound': 4}]

```

## City events

```json
[{'type': 'antiVaccinationism', 'sinceRound': 22}]
[{'type': 'bioTerrorism', 'round': 59, 'pathogen': {'name': 'Azmodeus', 'infectivity': 'o', 'mobility': 'o', 'duration': 'o', 'lethality': 'o'}}]
```