# Das GitLab-Repo der Gruppe Quarantäne Kapitäne

## Dokumentation
Die Dokumentation für unser Projekt ist in dieser [PDF](https://git.imp.fu-berlin.de/phup/pandemie/blob/master/2020_InformatiCup_Dokumentation.pdf) zu finden.

## Testen unserer Lösung
Wenn sie unsere Lösung testen wollen, können sie dies mit dem zusätzlichen Parameter -u gefolgt von der Amazon Gateway API URL. Dies sieht exemplarisch so aus:

`./ic20_linux -u https://nkv32d1063.execute-api.us-east-1.amazonaws.com/default/pandemicGame`

Den Code, der unsere Logik enthält und welcher auf Amazon AWS Lambda deployed ist, finden sie [hier](https://git.imp.fu-berlin.de/phup/pandemie/blob/master/L%C3%B6sung/final_alg_amazon.py).